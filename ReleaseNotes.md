﻿# Worker Safety Application Release Notes

### V1.0.12.1a - 231209
1. Updated CT1XXX firmware reference to v2.4.4.2a
 - Fixed TimeSync bug (KP-350)
 - Various RTC Kernel Fixes
 - NETWORK RESET operation now rejoins instead of clearing NVM params
 - DUX12 accelerometer bug fixes
2. LoraWAN v1.04 radio stack
  - US915 SB2 / ADR Disabled
  - EU868 Standard Band / ADR enabled

### V1.0.11.0 - 230919
1. Firmware Reference v2.3.0.0
   * Incorporates all stabilization improvements.  This is a nearly final release candidate for the firmware.
2. Flex Platform: v1.4.0.2
3. Standard EU band ADR enabled.

### V1.0.10.0, 230502
1. Firmware reference v2.1.1.1a
   * Changed I2C memory bus from 100Khz to 400Khz.
   * Fixed accelerometer and i2c memory bugs.
   * Changed BVT LED behavior (manufacturing).
2. Platform version v1.3.1.4


### V1.0.9.0 230417
1. Updated Firmware Bundle to V2.0.15.0a
   - Reworked positioning control to ensure OPL is shutdown to conserve power.
   - Includes updates from alpha bundle v2.0.14.0a.
### V1.0.8.2 230125 ###
- Build v1.0.8.2 230125
  1. Fixed wrong API specification to v1.1.1.0.
- Build v1.0.8.1 230125
  1. References updated v2.0.10.3 build (fix bad compile)
- Build v1.0.8.0 (BAD BUILD) 230118 --DEPRECATED
  1. References updated v2.0.10.2
     - Implements production support for Temperature and Battery ADC use
  2. Changed default polling interval to 4 hours to match documentation
  3. Fixed polling interval disable bug
  4. Changed TEMP version as follows:
     - Disabled check-in/check-out functionality
     - Tracking always enabled
  5. Updated v1 lib to v1.2.13.0

### V1.0.7.1a 230118 ###
- Build v1.0.7.1a  230118
  1)  References updated v2.0.10.1a Bundle supporting both battery and temperature reporting 
  2)  Implemented support for temperature bias calibration and higher resolution temperature reporting (0.01 degrees C).
      However, accuracy and uncertainty remain about ~1.0 degrees Celsius with some improvement due to averaging.

- Build v1.0.7.0a  230103
  1)  References updated v2.0.10.0a Bundle supporting Temperature reporting hack
  2)  Implements temperature reporting functions in Worker.p.

### V1.0.6.1 230118 ###
- Build v1.0.6.1 230118
    1)  References updated firmware bundle to v2.0.9.1
        * Fixes hibernation power drain issue.
- Build v1.0.6.0 230203
    1)  References updated firmware bundle to v2.0.9.0
        * Fixes foundation bug.

### V1.0.5.1 221228 ###
- build v1.0.5.1
  1. Bugfix: had battery and locate buttons swapped.  Battery status is now button #2 and locate is button #1.
- build v1.0.5.0
  1.  References updated v2.0.8.4 firmware bundle supporting manufacturing BVT bugfixes.
  2.  Uses V1 Library v1.2.11.1, which increases motion tracker default sensitivity, now 50mg.
  3.  Added Battery Status Indicator on button #2 (1 quick press).
  4.  Restricted pro location mode to just button #2 (1 quick press).

### V1.0.4.0 221209 ###
1) Updated firmware bundle to v2.0.7.0
2) Implemented API updates for V1 library version 1.2.10.0
3) Added Pro edition compile and publish options with additional button commands and modified defaults. See readme file for details.

### V1.0.3.0 221006 ###
1) Updated firmware bundle to v2.0.6.4
2) Added Auto checkout functionality when Out of network (OON) for extended periods of time.
3) Changed worker check-in/check-out operation.  90 seconds after Checkout the LoRa Radio will be disabled.  Check-in will rejoin.

### V1.0.2.3 221003 ###
- Build 3 (221004)
  1) Updated firmware bundle to v2.0.6.3
     * Fixed set Factory Hibernate issue
	
-  Build 2 (221003)
   1) Updated firmware bundle to v2.0.6.2
      * Fixed Authorization issue
      * Improved reliability or Telemetry Enable/Disable.   

- Build 1a (220930)
  1) Updated firmware bundle to v2.0.6.1a 
     * Fixes out of network issue.

- Build 0a (220927)
  1) References updated v2.0.6.0a Bundle & STM Firmware using the V2 Stack. 
  	* New Telemetry retry features with better roaming performance.
### V1.0.1.0a_v2stack 220927 ###
1) V2 Stack Build
    *  Implemented with library version v1.2.8.0.
2) Changed worker check-in/check-out operation.  60 seconds after Checkout the LoRa Radio will be disabled.  Check-in will rejoin.
---

### V1.0.0.1a_v2stack 220923 ###
1) V2 Stack Build
    *  Implemented with library version v1.2.7.1.
---

### V1.0.0.1 220817 ###
1)  Initial Implementation
    *  Implemented with library version v1.2.7.1.
---
*Copyright 2022-2023, Codepoint Technologies, Inc.* 
*All Rights Reserved*
