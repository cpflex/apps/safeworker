@ECHO OFF
if [%1]==[] goto usage

echo ** Compiling / Publishing  Application Standard Edition
call compile %1
call cphub_push %1

echo ** Compiling / Publishing  Application Pro Edition
call compile %1 PRO
call cphub_push %1 PRO

echo ** Compiling / Publishing  Application Temperature Edition
call compile %1 TEMP
call cphub_push %1 TEMP

goto :eof
:usage
@echo Usage: %0 ^<tag (e.g. v1.0.2.3)^>
exit /B 1
