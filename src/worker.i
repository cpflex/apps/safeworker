/**
 *  Name:  worker.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019 Codepoint Technologies
 *  All Rights Reserved
 */

 #include "telemetry.i"

forward  WorkerInit();

/**
* @brief  Sets the checkin status.
*/
forward   ResultCode: WorkerSetStatus(int: nidStatus, bool: bForce = false);

/**
* @brief  Returns true if worker is checked in.
*/
forward   int: WorkerGetStatus();

/**
* @brief  Processes downlink received messages.
* @returns Returns the result code.
*/
forward  int: WorkerDownlink(Sequence: seqOut, MsgType: tmsg, id, Message: msg);

/**
* @brief  Returns the number of seconds to wait for auto checkout when Out of Network (OON) condition exists.
*  The default is 86400 seconds (24 hours).
*/
forward  int: WorkerGetOonCheckout( );

/**
* @brief Updated the temperature reporting interval with the specified value.
* @param interval the interval in minutes.  0 is disabled.
*/
forward WorkerUpdateTempReporting( interval = -1);

/**
* @brief Kills temperature reporting.
*/
forward WorkerKilTempReporting();