/**
*  Name:  ConfigTelemMgr.i
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2021 Codepoint Technologies, Inc.
*  All Rights Reserved
*
* Description:
*  Configuration constants for network link Checks.
**/

#include "ui.i"

#pragma warning disable 213

// Telemetry Status Indications.
const  INDICATE_LED_NORMAL   = LED3;
const  INDICATE_LED_CRITICAL = LED4;
const  INDICATE_DISABLED_ON   = 500;
const  INDICATE_DISABLED_OFF  = 250;
const  INDICATE_UNAVAILABLE_ON   = 750;
const  INDICATE_UNAVAILABLE_OFF  = 250;
const  INDICATE_RSS_ON      = 500;		
const  INDICATE_RSS_OFF     = 250;

const DEFAULT_POLLING = 240;  // Default polling interval is every 4 hours.

//NID Command Map, by default these map to system protocol spec.
//If your protocol is different, update the NID mappings.
const NIDTELEMMAP: {
	NM_TELEM_SETTINGS  = NID_SysSttgs, // Telemtry Settings..
	NM_TELEM_POLL_INTVL = NID_SysSttgsPollintvl, //Downlink / Health Polling Interval.
};

#pragma warning enable 213