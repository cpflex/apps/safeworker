/**
*  Name:  ConfigSystem.p
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2022 Codepoint Technologies, Inc.
*  All Rights Reserved
*
* Description:
*  Configuration constants for the battery module.
**/

/*************************
* Battery Configuration 
**************************/
#include "ui.i"
#include <OcmNidDefinitions.i>

#pragma warning disable 213
//NID Command Map, by default these map to system protocol spec.
//If your protocol is different, update the NID mappings.
const NIDSYSMAP: {
	NM_SYS_RESET = NID_SysReset,
	NM_ARCH_ERASE = NID_ArchiveErase,
	NM_ARCH_SYSLOG = NID_ArchiveSyslog,
	NM_ARCH_DISABLE = NID_ArchiveSyslogDisable,
	NM_ARCH_ALERT = NID_ArchiveSyslogAlert,
	NM_ARCH_INFO = NID_ArchiveSyslogInfo,
	NM_ARCH_DETAIL = NID_ArchiveSyslogDetail,
	NM_ARCH_ALL = NID_ArchiveSyslogAll
};

#pragma warning enable 213