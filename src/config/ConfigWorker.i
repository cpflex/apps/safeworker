/**
*  Name:  ConfigWorker.i
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2021 Codepoint Technologies, Inc.
*  All Rights Reserved
*
* Description:
*  Configuration constants for network link Checks.
**/

#include "ui.i"



// WORRKER Status Indications.
const  WORKER_IND_ON   = 500;
const  WORKER_IND_OFF  = 250;
const  WORKER_IND_COUNT  = 5;

#if defined( __TEMPDEMO_EDITION__)
const WORKER_TEMP_INTVL= 1;
const bool: WORKER_TCTRL_DEFAULT = false;
#else
const WORKER_TEMP_INTVL= 0;
const bool: WORKER_TCTRL_DEFAULT = true;
#endif

