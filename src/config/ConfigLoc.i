/**
*  Name:  ConfigLoc.i
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2022 Codepoint Technologies, Inc.
*  All Rights Reserved
*
* Description:
*  Configuration constants for the Location Configuration Module.
**/

/*************************
* Battery Configuration 
**************************/
#include "ui.i"
#include <OcmNidDefinitions.i>

//NID Command Map, by default these map to system protocol spec.
//If your protocol is different, update the NID mappings.
const NIDLOCMAP: {
	NM_LOCETTINGS  = _: NID_LocSttgs,
	NM_PMODE = _: NID_LocSttgsPmode,
	NM_PMODE_DEFAULT= _: NID_LocSttgsPmodeDefault,
	NM_PTECH = _: NID_LocSttgsPtech,
	NM_PTECH_AUTO = _: NID_LocSttgsPtechAutomatic,
};

//Configuration Settings
const PositioningMode: LOCCFG_DEFAULT_MODE = PM_default;		//Default location mode
const PositioningTechnologies: LOCCFG_DEFAULT_TECH = PT_automatic; 	//Default positioning technology.
const LOCCFG_COUNT_BLINK_MODE = 5;			//Number of blinks to indicate location config mode is active.
const LOCCFG_COUNT_INTVL_ON = 133; 			//Millisecond LED on for blink.
const LOCCFG_COUNT_INTVL_OFF = 200;			//Millisecond LED off for blink.