/**
 *  Name:  worker.p
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2022 Codepoint Technologies
 *  All Rights Reserved
 */
 #include "worker.i"
 #include "app.i"
 #include "ui.i"
 #include "MotionTracking.i"
 #include "emergency.i"
 #include "TelemMgr.i"

 #include "config/ConfigWorker.i"

#pragma warning disable 213
#pragma warning disable 203

/*******************************************************
* Forward Declarations.
*******************************************************/
forward ResultCode: _SendStatus(int: nidStatus);
forward @OnReportTemp( tick, epoch);

const int: DEFAULT_CHECKOUT_OON_INTVL = 86400;
const int: MAX_TEMP_INTERVAL = 10080;

/*************************
* Statics
*************************/
static int: _IntvlCheckoutOon = DEFAULT_CHECKOUT_OON_INTVL;

/*************************
* API
*************************/

/**
* @brief Initializes the Worker Status Module.
*/
WorkerInit( )
{
	new bool: bTrackCtrl = WORKER_TCTRL_DEFAULT;
	new int: nidStatus;

	if( !nvmrec_exists(NVREC_WORKER_STATUS)) {
		nvmrec_init(NVREC_WORKER_TCTRL, bTrackCtrl);
		nvmrec_init(NVREC_WORKER_STATUS, NID_SwWorkstatusCheckout);	
		nvmrec_init(NVREC_WORKER_CHECKOUT_OON, _IntvlCheckoutOon);
		nvmrec_init(NVREC_WORKER_TEMP_INTVL, WORKER_TEMP_INTVL);

		nidStatus  = NID_SwWorkstatusCheckout;
	} else {
		bTrackCtrl  = nvmrec_get(NVREC_WORKER_TCTRL);
		nidStatus  = nvmrec_get(NVREC_WORKER_STATUS);
		_IntvlCheckoutOon = int: nvmrec_get(NVREC_WORKER_CHECKOUT_OON );

	}

	Log("** Worker Status Module", MC_info, MP_med, LO_default );
	LogFmt("  status      : %d", MC_info, MP_med, LO_default, nidStatus);
	LogFmt("  trk ctrl    : %d", MC_info, MP_med, LO_default, bTrackCtrl);	
	LogFmt("  intvlChkOon : %d", MC_info, MP_med, LO_default, _IntvlCheckoutOon);	

	TelemRetryConfig( -1, _IntvlCheckoutOon + 1800);

#if defined( __TEMPDEMO_EDITION__)
	//Activate Tracking
	trk_Activate( );
	WorkerUpdateTempReporting();
#else
	//Reset to previous status.
	WorkerSetStatus(nidStatus, true);
#endif
	
}

/**
*  Sets the checkin status.
*/
stock  ResultCode: WorkerSetStatus(int: nidStatus, bool: bForce = false)
{
	new ResultCode: rc = RC_success;
	new bool: bCheckedIn = nvmrec_get(NVREC_WORKER_STATUS) == NID_SwWorkstatusCheckin;
	new bool: checkin = nidStatus == NID_SwWorkstatusCheckin;
	
	if( checkin != bCheckedIn || bForce) {					  
		nvmrec_update( NVREC_WORKER_STATUS, nidStatus);

		rc= _SendStatus( nidStatus);

		//If tracking control is enabled, activate/deactivate tracking.
		if( nvmrec_get(NVREC_WORKER_TCTRL)) {			
			if( checkin) { 
				TelemMgr_EnableRadio(true);
				trk_Activate( ); 				
			} else  if( !e911_IsEmergency()) {
				//Only deactivate if not in emergency mode.
				trk_Deactivate(); 
				TelemMgr_EnableRadio(false);
			}
		}		

		//Enable/disable temperature reporting.
		if(checkin) {
			WorkerUpdateTempReporting();
		} else {

		}

		//Display indicator if not forced.
		if( !bForce) {
			UiSetLed( LED1, LM_count, WORKER_IND_COUNT,WORKER_IND_ON,WORKER_IND_OFF);
			if( !checkin) {
				UiSetLed( LED2, LM_count, WORKER_IND_COUNT,WORKER_IND_ON,WORKER_IND_OFF);
			}
		}
	}
	return rc;
}

/**
* @brief  Returns true if worker is checked in.
*/
stock   int: WorkerGetStatus() {
	return int: nvmrec_get(NVREC_WORKER_STATUS);
}

/**
* Processes downlink received messages.
* returns the result code.
*/
stock  int: WorkerDownlink(Sequence: seqOut, MsgType: tmsg, id, Message: msg)
{
	new ct = 0;
	if( tmsg == MT_Int32Msg )	{		
		switch( id) {
			case NID_SwWorkstatus:	 {
				//Override tracking config and defer to our state machine for enable and disable.
				new  status;

				if( TelemRecv_ToInt32Msg( msg, status) == RC_success && status > 0 )
				{
					WorkerSetStatus( status);
				} else {
					_SendStatus( nvmrec_get(NVREC_WORKER_STATUS) );
				}
			}
		}
	} else if( tmsg == MT_KeyValueMsg && id == NID_SwWrksttgs )	{
		new tdata, key;
		new Data: data;
		new value;
		while( TelemRecv_NextKeyValueMsgValue( msg, key, tdata, data)  ==  RC_success)
		{
			//Only process ints and valuid data.
			if( tdata != DT_int || TelemRecvData_ToInt(data, value) != RC_success )
				continue;

			switch(key)
			{
				case NID_SwWrksttgsTrkctrl: {
					nvmrec_update( 
						NVREC_WORKER_TCTRL,
						value == NID_SwWrksttgsTrkctrlEnabled);
				}
				case NID_SwWrksttgsOoncheckout: {
					if( value > 604800) {
						value = 604800
					} else if( value < 300 ) {
						value = 300;
					}
					
					//Update the auto checkout interval.
					_IntvlCheckoutOon = value;
					TelemRetryConfig( -1, value + 1800);
					nvmrec_update( NVREC_WORKER_CHECKOUT_OON,value);
				}
				case NID_SwWrksttgsTempreport: {
					WorkerUpdateTempReporting( value);
				}
				case NID_SwWrksttgsTempbias: {
					SysTempBiasSet( value);
				}
			}
		}
	}
	return ct;
}

/**
* @brief  Returns the number of seconds to wait for auto checkout when Out of Network (OON) condition exists.
*  The default is 86400 seconds (24 hours).
*/
stock  int: WorkerGetOonCheckout( ) {
	return _IntvlCheckoutOon;
}

/**
* @brief Updated the temperature reporting interval with the specified value.
* @param interval the interval in minutes.  0 is disabled.
*/
stock WorkerUpdateTempReporting( interval = -1) {
	//Update if not using default.
	if( interval > -1) {
		if( interval > MAX_TEMP_INTERVAL) {
			interval = MAX_TEMP_INTERVAL;
		}
		nvmrec_update( NVREC_WORKER_TEMP_INTVL, interval);		
	} else  {
		//Use default value.
		interval = nvmrec_get(NVREC_WORKER_TEMP_INTVL);
	}

	KillRtcAlarm( "@OnReportTemp");

	if( interval > 0 )  {
		TraceDebugFmt( "Activating Temperature Reporting: %d minutes", interval);
		SetRtcAlarm( Now() + interval, interval*60, TIMER_INFINITE_CYCLES, "@OnReportTemp");		
	} else {                    
		TraceDebug( "Deactivating Temperature Reporting");
	}
}

/**
* @brief Kills temperature reporting.
*/
stock WorkerKilTempReporting() {
	KillRtcAlarm( "@OnReportTemp");
	TraceDebug( "Deactivating Temperature Reporting");
}

/*************************
* Interal functions
**************************/

/**
* @brief Temperature report alarm handler.  Uplinks current temperature data.
*/
@OnReportTemp( tick, epoch) {
	new ResultCode: rc;
	new temperature = 0;

	//Get the current system temperature and report it.
	if( SysTempGet( temperature) == RC_success) {
		TraceDebugFmt( "Reporting Temperature: %d (0.01 degrees Celsius)", temperature);

		//Post Update.
		new Sequence:seqOut;
		if( (rc = TelemSendSeq_Begin( seqOut, 100)) == RC_success) {
			rc = TelemSendInt32Msg( 
				seqOut, 
				NID_SwTemperature, 
				temperature,
				MP_med, MC_info);
		} 

		//Send it confirmed.
		TelemSendSeq_End( seqOut, rc != RC_success, TPF_CONFIRM_ARCHIVE);	
	}
}

static ResultCode:  _SendStatus(int: nidStatus)
{
	new ResultCode: rc;

	//Post Update.
	new Sequence:seqOut;
	if( (rc = TelemSendSeq_Begin( seqOut, 100)) == RC_success) {
		rc = TelemSendInt32Msg( 
			seqOut, 
			NID_SwWorkstatus, 
			nidStatus,
			MP_med, MC_info);
	} 

	//Send it confirmed.
	TelemSendSeq_End( seqOut, rc != RC_success, TPF_CONFIRM_ARCHIVE);	
	return rc;
}
#pragma warning enable 203
#pragma warning enable 213