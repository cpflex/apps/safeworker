/*******************************************************************************
*                !!! OCM CODE GENERATED FILE -- DO NOT EDIT !!!
*
* Safe Worker Application Protocol
*  nid:        SafeworkerAppProtocol
*  uuid:       5072e9a3-f202-4f43-9c66-7c2e12f89fb9
*  ver:        1.1.1.0
*  date:       2023-01-18T23:12:26.777Z
*  product: Cora Tracking (CT) Series CT1000
* 
*  Copyright 2022 - Codepoint Technologies, Inc. All Rights Reserved.
* 
*******************************************************************************/
const OcmNidDefinitions: {
	NID_ArchiveSyslog = 1,
	NID_ArchiveSyslogDisable = 2,
	NID_ArchiveSyslogAlert = 3,
	NID_ArchiveSyslogInfo = 4,
	NID_ArchiveSyslogDetail = 5,
	NID_ArchiveSyslogAll = 6,
	NID_ArchiveErase = 7,
	NID_SwBattery = 8,
	NID_SwCharger = 9,
	NID_SwChargerCharging = 10,
	NID_SwChargerDischarging = 11,
	NID_SwChargerCritical = 12,
	NID_SwChargerCharged = 13,
	NID_SwEmergency = 14,
	NID_SwEmergencyDisable = 15,
	NID_SwEmergencyEnable = 16,
	NID_SwTracking = 17,
	NID_SwTrackingDisabled = 18,
	NID_SwTrackingEnabled = 19,
	NID_SwTrackingActive = 20,
	NID_SwTrksttgs = 21,
	NID_SwTrksttgsAcquire = 22,
	NID_SwTrksttgsEmrintvl = 23,
	NID_SwTrksttgsInactivity = 24,
	NID_SwTrksttgsNomintvl = 25,
	NID_SwWorkstatus = 26,
	NID_SwWorkstatusCheckin = 27,
	NID_SwWorkstatusCheckout = 28,
	NID_SwWrksttgs = 29,
	NID_SwWrksttgsTrkctrl = 30,
	NID_SwWrksttgsTrkctrlEnabled = 31,
	NID_SwWrksttgsTrkctrlDisabled = 32,
	NID_SysReset = 33,
	NID_SysSttgs = 34,
	NID_SysSttgsPollintvl = 35,
	NID_LocSttgs = 36,
	NID_LocSttgsPmode = 37,
	NID_LocSttgsPmodeDefault = 38,
	NID_LocSttgsPmodeCoarse = 39,
	NID_LocSttgsPmodeMedium = 40,
	NID_LocSttgsPmodePrecise = 41,
	NID_LocSttgsPtech = 42,
	NID_LocSttgsPtechAutomatic = 43,
	NID_LocSttgsPtechWifi = 44,
	NID_LocSttgsPtechBle = 45,
	NID_LocSttgsPtechWifible = 46,
	NID_SwWrksttgsOoncheckout = 47,
	NID_SwWorkstatusCheckoutoon = 48,
	NID_SwWrksttgsTempreport = 49,
	NID_SwTemperature = 50,
	NID_SwWrksttgsTempbias = 51
};
