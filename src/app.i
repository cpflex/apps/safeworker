/**
 *  Name:  app.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019 Codepoint Technologies
 *  All Rights Reserved
 */


/**
* @brief Records defined to manage persisted application data.
*/
const NvmAppRecords: {
    NVREC_WORKER_TCTRL  = 0x1, //Tracking Control Status.
	NVREC_WORKER_STATUS	= 0x2, // Worker Status.
    NVREC_WORKER_CHECKOUT_OON = 0x3, //OON Auto Checkout Time.
    NVREC_WORKER_TEMP_INTVL=0x4, //Temperature Reporting Interval.

}


/**
* @brief  Application Defined callback handler that indicates entering power savings.
* @remarks Required implementation to catch enter power saving events.
*/
forward app_EnterPowerSaving(pctCharged);

/**
* @brief  Application Defined callback handler that indicates entering power savings.
* @remarks Required implementation to catch exit power saving events.
*/
forward app_ExitPowerSaving(pctCharged);
