@ECHO OFF
if [%1]==[] goto usage
set EDITION=
set lib=../../v1/lib
if [%2]==[PRO] (set EDITION=__PRO_EDITION__=1)
if [%2]==[TEMP] (set EDITION=__TEMPDEMO_EDITION__=2)

set _DATE=%date%
set _TIME=%time%
set _VERSION=%1

REM Display Header.
ECHO Compiling Worker Safety Application: 
ECHO Version: %_VERSION%
ECHO Date: %_DATE%
ECHO Time: %_TIME%
ECHO Edition: %EDITION%
 

REM Create Version File with string data.
ECHO stock const __VERSION__{} = "%_VERSION%"; > src/version.i
ECHO stock const __DATE__{} = "%_DATE%"; >>		 src/version.i
ECHO stock const __TIME__{} = "%_TIME%"; >>		 src/version.i

pawncc ^
	%lib%/Battery.p %lib%/MotionTracking.p %lib%/LocationConfig.p %lib%/SysMgr.p ^
	%lib%/NvmRecTools.p %lib%/TelemMgr.p  worker.p emergency.p app.p ^
	-Dsrc -S256 -X32768 -XD4092 ^
	-i. -i../../v1/include -i%lib%  ^
	-o../safeworker.bin ^
	%EDITION%	
	

goto :eof
:usage
@echo Usage: %0 ^<version^> [PRO|TEMP]
exit /B 1

