# CT1000 (N100) - Worker Safety Application

**Version: 1.0.12.0a<br/>**
**Release Date: 2023/12/06**

This Flex application provides basic functions useful for employee safety and monitoring
applications in a multi building campus environment, where the tag is worn or stored 
in an RF friendly accessory such as a purse, backpack, jacket pocket.   

This readme file covers the following editions of Worker Safety:

- **Worker Safety Standard** - published as .../nali/applications/safeworker in CP-Flex Hub.  This application
  supports the basic user interface with capabilities to check-in, check-out, and activate emergency mode.  
- **Worker Safety Pro**      - published as .../nali/applications/safeworker-pro in CP-Flex Hub.  Pro edition 
  adds additional user interface capabilities and modifies default tracking intervals.
- **Worker Safety Temperature Demo** - published as .../nali/applications/safeworker-tempdemo in CP-Flex Hub.  Temperature demonstration edition 
  adds 1 minute temperature reporting intervals to Pro edition.
  
## Key Features  
The tracker application is designed to provide useful tracking and event 
information as an individual goes about their daily activities.

### Standard Edition Features
1. User activated emergency mode 
2. Motion activated location data collection
3. Cloud configurable tracking settings / report
4. Out of network message caching 
5. Battery status events and report
6. Location measurement configuration
7. Configurable health check reporting (4 hours default)
8. Check In/ Check Out with Location Report (configurable)
9. Remote Tracking Enable / Disable
10. Remote reset device (development only)
11. Automatic checkout when out of network for extended periods of time (default is 24 hours)
12. Default reporting interval:  10 minutes normal mode / 1 minute emergency mode

### Pro Edition Features
1. All standard edition features
2. User emergency mode deactivation
3. User 'Locate Now' function.  
4. Default reporting interval:  2 minutes normal mode / 1 minute emergency mode

## Settings ##

### Cloud Configurable Non-Volatile Settings
The application supports non-volatile downlink settings for standard reporting interval.  These values survives reboot or dead battery conditions. For more information on configuring the device see the [Safeworker Communication Specification](./appapi/SafeWorker.ocm.json)

### Out of Network Link Check Settings
Network link Checks are performed using a combination of time and activity metrics.  Uplink checks are performed
in accordance to the following schedule.   If confirmed messages are sent, the schedule is updated.

|  Link Check Metric                 |      Value            |
|:-----------------------------------|:---------------------:|
|   Stationary Time Interval         |   every 720 minutes (12 hrs)    |
|   Stationary Uplink Activity       |   every 5 messages    |
|   Active Tracking Time Interval    |   every 10 minutes    |
|   Active Tracking Uplink Activity  |   every 3 messages    |

These settings can be adjusted by implementing a custom ConfigLinkCheck.i

### Out of Network (OON) Caching Settings
The firmware now supports OON caching of messages, if the link check fails messages marked with the TPF_ARCHIVE
flag, will be stored until the metric becomes available.   Once the network is available at DR3 or higher, the
device will push three messages per minute until the archive is empty.  This is in addition to any 
messages stored in the uplink queue, which may contain up to five (5) messages.

### Battery Status Reports
The tag will report battery status when connect or disconnect from charging. The battery level and temperature are monitored every 
10 minutes. The tag will also report battery status when its level change more than 5%. The tag will go to low power mode and 
stop automatic location reporting when battery level is below 20%. The tag will resume automatic location reporting after battery 
level return to above 25%. 

# User Interface 

## Normal Mode
Normal operating mode is entered when the device boots. The tracking is deactivated and can be enabled by user executing the "Check In" command.  Positioning mode is configurable by the cloud application supporting either coarse, medium, or precise data collection modes.   Coarse positioning is the most battery efficient, and precise is least efficient.

*Note:  If the tag is on the charger when booting, the entry to normal operating mode will be delayed 2 minutes*

### Button Commands

The following table summarizes the user interface in normal operating mode.  Button #1 is the top button and button #2 is the bottom button nearest the charger pads.

| **Command**                         |     **Action**       |     **Button**     |     **Edition**     | **Description**           |
|-------------------------------------|:--------------------:|:------------------:|:------------------:|---------------------------|
| **Check In**                        | double (2) <br>quick presses      | any button or both | ALL   | Sends a check-in message  |
| **Check Out**                       | triple (3) <br>quick presses      | any button or both | ALL   | Sends a check-out message |
| **Activate <br>Emergency Mode<br>** | hold > 3 <br>seconds              | any button or both | ALL   | Activates emergency mode  |
| **Deactivate  <br>Emergency Mode<br>** | quick press five <br>(5) times | any button or both | PRO   | Deactivates emergency mode |
| **Locate Now**                      | single (1) <br>quick press        | button #1          | PRO   | Initiates an immediate location request  |  
| **Battery Status**                  | single (1) <br>quick press        | button #2          | ALL   | Indicates battery level <br> [*See Battery Indicator*](#battery-indicator)  |  
| **Network Reset**                   | hold > 15<br>seconds              | any button or both | ALL   | Network reset device      |
| **Factory Reset**                   | hold > 25<br>seconds              | any button or both | ALL   | Resets the network and configuration to factory defaults for the application.     |

*Note:  60 seconds after a "Check Out" command the radio will disabled.*

### LED Indicators

The following table indicates the meaning of the two bi-color LEDs on the device.  The LED's are
positioned:

* **LED #1** - Upper LED, furthest away from charger connector
* **LED #2** - Lower LED, closest to charger connector


| **Description**                |                **Indication**               | **Edition** |   **LEDS**  |
|--------------------------------|:-------------------------------------------:|:-----------:|:-----------:|
| **Device Reset**               |       Green blink <br>three (3) times       |     ALL     |     both    |
| **Locate Initiated**           |        Green blink <br>two (2) times        |     PRO     |    LED #1   |
| **'Check In'**                 |     Green blink five<br>(5) times slowly    |     ALL     |    LED #1   |
| **'Check Out'**                |    Orange blink five<br>(5) times slowly    |     ALL     | LED #1 & #2 |
| **Emergency Mode Activated**   |      Flashing Red<br>three (3) seconds      |     ALL     |    LED #1   |
| **Emergency Mode Active**      |  Red blink once every <br>five (5) seconds. |     ALL     |    LED #1   |
| **Emergency Mode Deactivated** |      Flashing green<br>five (5) seconds     |     ALL     |    LED #1   |
| **Battery Charging**           | Orange slow blink every<br>ten (10) seconds |     ALL     |    LED #2   |
| **Battery Fully Charged**      |  Green slow blink every<br>ten (10) seconds |     ALL     |    LED #2   |
| **Invalid Input**              |  Red blink <br>three (3) times quickly      |     ALL     |    LED #2   |
 
## Emergency Mode 

Emergency mode provides a panic button feature, where when the user holds either or both buttons for more than
three (3) seconds, the emergency mode is toggled.  When activated, it sends an emergency activated message to 
the cloud and begins tracking the user. The specified emergency tracking interval can be modified by the cloud services.

Standard edition emergency mode can only be deactivated by the cloud.  With the pro-edition, emergency mode can be deactivated by the user.

## Battery Indicator ##

Quickly pressing Button #2 will indicate the percent charge of the battery in 20% increments.

LED #2 will blink as follows to indicate percent charge.

| LED #2  | % Charge    | Description|
|---------|-------------|------------|
|  1 red  |   < 5%      | Battery level is critical,  charge immediately. |
|  1 green |  5%  - 30%  | Battery low  less than 30% remaining. |
|  2 green |  30% - 50%  | Battery is about 40% charged. |
|  3 green |  50% - 70%  | Battery is about 60% charged. |
|  4 green |  70% - 90%  | Battery  is about 80% charged. |
|  5 green |  > 90%      | Battery is fully charged. |

The tag will report battery status when connect or disconnect from charging. The battery level and temperature are monitored every 
10 minutes. The tag will also report battery status when its level change more than 5%. The tag will go to low power mode and 
stop automatic location reporting when battery level is below 20%. The tag will resume automatic location reporting after battery 
level return to above 25%. 

## Network status ##
When the tag leaves network coverage area, it will try to connect to the network when there is a message needs 
to be uploaded to the cloud. The tag will save its scanning data when it is out of the network. The saved location data will be sent to the cloud when the tag comes back to the network area. If the tag is not moving, it will check the network status according to loRaWAN standard for joining in the region.

### Out of network (OON) / Out of Coverage area ###
When the tag moves out of LoRaWAN network coverage area, it will store at least 2000 location messages in the memory.
When the tag comes back to the network coverage area, it will upload stored messages based on customer requirements. The upload
function will be defined based on customer application.

When the device is out of the network coverage area, it will slow its attempts to reconnect to the network over time to save power. 

RETRY SCHEDULE

| Interval |        **Uplink Offset**       | **Comment**                                                          |
|:--------:|:------------------------------:|----------------------------------------------------------------------|
|  **10**  |   < tLastConfirmedUplink + 60  | Attempt every FLEX_TELEM_DEFAULT_MINUPLINK_SEC seconds for 1 minute. |
|  **120** |  < tLastConfirmedUplink + 1800 | Attempt every 2 minutes for 30 minutes.                              |
|  **600** | < tLastConfirmedUplink + 36000 | Attempt every 10 minutes for 10 hours.                               |
| **1200** | < tLastConfirmedUplink + 86400 | Attempt every 20 minutes for 24 hours.                               |


REJOIN SCHEDULE (Retries first before rejoin)

| **Interval (sec)** |   **state**   | **Time Period (sec)**    | **Comment**                                |
|:------------------:|:-------------:|--------------------------|--------------------------------------------|
|        **0**       | > unavailable | ctCache = 0              | No data pending.                           |
|       **10**       | > unavailable | ctCache > 1              | Minimum uplink interval.                   |
|       **120**      | > disabled    |                          | Cycles no action                           |
|       **30**       | > disabled    | < tUnavailable + 60      | Attempt 2 times for first 60 seconds.      |
|      **1800**      | > disabled    | < tUnavailable + 3600    | Attempt every 30 minutes up to an hour.    |
|      **4200**      | > disabled    | < tUnavailable + 36000   | Attempt every 1.25  hours up to 10  hours. |
|      **28800**     | > disabled    | < tUnavailable + 86,400  | Attempt every 8  hours up to 24 hours.     |
|      **43200**     | > disabled    | < tUnavailable + 604,800 | Attempt every 12  hours up to 1 week.      |
|      **86400**     | > disabled    | > tUnavailable + 604,800 | Attempt every 24 hours until battery dies. |

## Source Code ##

The source code for the pedestrian tracker is found in the `src` folder and is organized 
into the following module files:
   * **src/app.p/app.i** -- main application system events, communications, and UI control.
   * **src/battery.p,battery.i** -- Battery and power management functions
   * **src/emergency.p,emergency/i** -- Emergency management functions
   * **src/tracking.p.tracking.i** -- Tracking controller.
   * **appapi/safeworker.ocm.json** -- OCM file defines commands and data structures supported by the device.  CP-Flex translates data from the device into these structures to present to the cloud application.
   * **OCMNidDefinitions.i** -- OCM Code Generated NID definitions for CP-Flex communications.   Derived from the PedTrackerBasic.ocm.json
   
To develop your own worker safety application, copy the entire folder to your own development
area. 

---
*Copyright 2022 Codepoint Technologies, Inc.* 
*All Rights Reserved*
