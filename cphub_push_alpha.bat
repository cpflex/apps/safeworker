@ECHO OFF
if [%1]==[] goto usage

set specfile="hub_spec.json"
set tag=%1
set edition=
if [%2]==[PRO] (set edition=-pro)
if [%2]==[TEMP] (set edition=-tempdemo)

ECHO Pushing Worker Safety Application alpha version %1 %2
cphub push -v -s %specfile% cpflexapp "./safeworker.bin" nali/applications/safeworker%edition%:%tag%
rem cphub push -v  application/text "README.md" nali/applications/safeworker:readme
goto :eof
:usage
@echo Usage: %0 ^<tag (e.g. v1.0.2.3)^>
exit /B 1
